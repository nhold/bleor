extends KinematicBody2D

export var speed:float
var playerName:String = "John"

func _ready():
	$InterfaceLayer/VBoxContainer/NameLabel.text = playerName

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var vector = Vector2.ZERO
	
	if Input.is_action_pressed("ui_left"):
		vector.x -= 1
	
	if Input.is_action_pressed("ui_right"):
		vector.x += 1
		
	if Input.is_action_pressed("ui_up"):
		vector.y -= 1
	
	if Input.is_action_pressed("ui_down"):
		vector.y += 1
	
	move_and_slide(vector.normalized() * speed)
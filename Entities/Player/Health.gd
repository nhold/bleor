extends Node

var maximumHealth:float = health
var health:float setget SetHealth
var healthRegeneration:float = 0.25

signal health_updated(health)

func SetHealth(newHealth):
	health = newHealth
	emit_signal("health_updated", health)